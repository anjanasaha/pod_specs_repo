Pod::Spec.new do |s|

  s.platform = :ios
  s.ios.deployment_target = '13.0'
  s.name = "Applaunch"
  s.summary = "Applaunch bootstraps the application."
  s.requires_arc = true

  s.version = "0.5.5"
  s.homepage = 'https://anjanasaha@bitbucket.org/anjanasaha/applaunch.git'
  s.license = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author = { 'anjanasaha' => 'anjanasaha26@gmail.com' }
  s.source = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/applaunch.git', :tag => 'v' + s.version.to_s }
  s.vendored_frameworks = 'APIGuard.framework'

  # 7
  s.framework = "UIKit"

  # Private
  s.dependency 'UserInterface'
  s.dependency 'RIBs', '0.1.11'
  s.dependency 'AuthenticationUI'

  s.source_files = "Applaunch/**/*.{swift}"
  s.swift_version = "5.0"

end
