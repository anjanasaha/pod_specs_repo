Pod::Spec.new do |s|

  s.platform = :ios
  s.ios.deployment_target = '13.0'
  s.name = "Applaunch"
  s.summary = "Applaunch bootstraps the application."
  s.requires_arc = true

  s.version = "0.1.14"
  s.homepage = 'https://anjanasaha@bitbucket.org/anjanasaha/applaunch.git'
  s.license = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author = { 'anjanasaha' => 'anjanasaha26@gmail.com' }
  s.source = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/applaunch.git', :tag => 'v' + s.version.to_s }

  # 7
  s.framework = "UIKit"

  # Private
  s.dependency 'UserInterface', '0.1.12'
  s.dependency 'RIBs', '0.1.11'

  s.source_files = "Applaunch/**/*.{swift}"
  s.swift_version = "5.0"

end
