Pod::Spec.new do |s|
    s.platform = :ios
    s.ios.deployment_target = '13.0'
    s.name = "StoringCommon"
    s.summary = "Storing Common hosts storage related declarations"
    s.requires_arc = true
    s.version = "0.0.1"
    s.homepage         = 'https://anjanasaha@bitbucket.org/anjanasaha/storingcommon.git'
    s.license          = { :type => 'MIT', :file => 'LICENSE.txt' }
    s.author           = { 'anjanasaha' => 'anjanasaha26@gmail.com' }
    s.source           = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/storingcommon.git', :tag => 'v' + s.version.to_s }
    s.dependency 'RxSwift'
    s.dependency 'CommonLib'
    s.source_files = "StoringCommon/**/*.{swift}"
    s.swift_version = "5.0"

end
