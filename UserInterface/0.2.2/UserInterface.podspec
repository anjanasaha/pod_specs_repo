Pod::Spec.new do |s|

    s.platform = :ios
    s.ios.deployment_target = '13.0'
    s.name = "UserInterface"
    s.summary = "UserInterface provides custom UI elements."
    s.requires_arc = true
    s.version = "0.2.2"

    s.license = { :type => "Apache", :file => "LICENSE" }
    s.author = { "Anjana Saha" => "anjanasaha26@gmail.com" }
    s.homepage = "https://anjanasaha@bitbucket.org/anjanasaha/userinterface.git"
    s.source = { :git => "https://anjanasaha@bitbucket.org/anjanasaha/userinterface.git",
                 :tag =>  'v' + s.version.to_s }

    s.framework = "UIKit"
    s.dependency 'SnapKit', '5.0.1'
    s.dependency 'lottie-ios', '3.1.8'

    s.dependency 'RIBs', '0.1.11'
    s.dependency 'CommonLib', '0.1.12'

    s.source_files = "UserInterface/**/*.{swift}"
    s.swift_version = "5.0"

end
