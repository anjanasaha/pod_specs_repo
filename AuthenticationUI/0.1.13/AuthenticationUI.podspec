Pod::Spec.new do |s|
    s.platform = :ios
    s.ios.deployment_target = '13.0'
    s.name = "AuthenticationUI"
    s.summary = "Authentication user interface"
    s.requires_arc = true
    s.version = "0.1.13"
    s.homepage         = 'https://anjanasaha@bitbucket.org/anjanasaha/authentication-ui.git'
    s.license          = { :type => 'MIT', :file => 'LICENSE.txt' }
    s.author           = { 'anjanasaha' => 'anjanasaha26@gmail.com' }
    s.source           = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/authentication-ui.git', :tag => 'v' + s.version.to_s }
    s.dependency 'RxSwift'
    s.dependency 'AuthenticationCommon', '0.1.13'
    s.dependency 'CommonLib', '0.1.12'
    s.dependency 'UserInterface', '0.1.12'
    s.dependency 'RIBs', '0.1.11'
    s.source_files = "AuthenticationUI/**/*.{swift}"
    s.swift_version = "5.0"

end
