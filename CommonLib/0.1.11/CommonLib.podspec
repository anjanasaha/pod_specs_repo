Pod::Spec.new do |s|

    s.platform = :ios
    s.ios.deployment_target = '13.0'
    s.name = "CommonLib"
    s.summary = "CommonLib has common code."
    s.requires_arc = true
    s.version = "0.1.11"
    s.license = { :type => "Apache", :file => "LICENSE.txt" }
    s.author = { "Anjana Saha" => "anjanasaha26@gmail.com" }
    s.homepage = "https://anjanasaha@bitbucket.org/anjanasaha/commonlib.git"
    s.source = { :git => "https://anjanasaha@bitbucket.org/anjanasaha/commonlib.git", :tag =>  'v' + s.version.to_s }
    s.framework = "Foundation"
    s.source_files = "CommonLib/**/*.{swift}"
    s.swift_version = "5.0"

end
