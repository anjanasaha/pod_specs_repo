Pod::Spec.new do |s|
    s.platform = :ios
    s.ios.deployment_target = '13.0'
    s.name = "AuthenticationCommon"
    s.summary = "Authentication Common hosts declarations"
    s.requires_arc = true
    s.version = "0.3.3"
    s.homepage         = 'https://anjanasaha@bitbucket.org/anjanasaha/authenticationcommon.git'
    s.license          = { :type => 'MIT', :file => 'LICENSE.txt' }
    s.author           = { 'anjanasaha' => 'anjanasaha26@gmail.com' }
    s.source           = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/authenticationcommon.git', :tag => 'v' + s.version.to_s }
    s.dependency 'RxSwift'
    s.source_files = "AuthenticationCommon/**/*.{swift}"
    s.swift_version = "5.0"

end
